import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

//Put Game data together after the game is finished and calculate tables after groupstage and semifinal
public class EntryFrame extends JFrame{
	
	private int counter;
	private int semi;
	private Team left;
	private Team right;
	private int[][] dv;
	private JTextField rightG;
	private JTextField leftG;
	private JButton ok;
	
	
	public EntryFrame(Team left, Team right, int counter, int [][] dv, int semi){
		this.left = left;
		this.right = right;
		this.dv = dv;
		this.counter = counter;
		this.semi = semi;
		
		setVisible(true);
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setLayout(null);
		setSize(300, 300);

		JLabel l = new JLabel(left.getName() + " : " + right.getName());
		l.setBounds(20, 20, 200, 30);
		add(l);

		leftG = new JTextField();
		leftG.setBounds(50, 70, 50, 30);
		add(leftG);

		rightG = new JTextField();
		rightG.setBounds(150, 70, 50, 30);
		add(rightG);

		ok = new JButton("OK");
		ok.setBounds(50, 200, 200, 50);
		ok.addActionListener(new OKListener());
		add(ok);
	}
	
	private class OKListener implements ActionListener{
		
		@Override
		public void actionPerformed(ActionEvent arg0) {
			int lg = 0;
			int rg = 0;
			while(true){
				try{
					lg = Integer.parseInt(leftG.getText());
					rg = Integer.parseInt(rightG.getText());
					if(lg <0 || rg <0)
						continue;
					break;
				}catch(NumberFormatException e){
					System.err.println("Wrong try again.");
					return;
				}
			}
			left.setGoals(left.getGoals() + lg);
			left.setMinusgoals(left.getMinusgoals() + rg);
			right.setGoals(right.getGoals() + rg);
			right.setMinusgoals(right.getMinusgoals() + lg);
			
			if(counter < 13){
				if(lg == rg){
					left.setPoints(left.getPoints() + 1);
					right.setPoints(right.getPoints() + 1);
				}else if(lg > rg){
					dv[left.getNummer()][right.getNummer()] = -1;
					dv[right.getNummer()][left.getNummer()] = 1;
					left.setPoints(left.getPoints() + 3);
				}else{
					dv[left.getNummer()][right.getNummer()] = 1;
					dv[right.getNummer()][left.getNummer()] = -1;
					right.setPoints(right.getPoints() + 3);
				}
			}else if(semi == 1 && counter <16){
				int win = 0;
				if(lg == rg){
					String[] ops = {left.getName(), right.getName()};
					win = JOptionPane.showOptionDialog(null, "Wer hat gewonnen?", null, 0, 0, null, ops, null);
				}else if(lg > rg || win == 0){
					left.setValue(6);
					right.setValue(4);
				}else if(rg > lg || win ==1){
					left.setValue(4);
					right.setValue(6);
				}
			}else{
				int win = 0;
				if(lg == rg){
					String[] ops = {left.getName(), right.getName()};
					win = JOptionPane.showOptionDialog(null, "Wer hat gewonnen?", null, 0, 0, null, ops, null);
				}else if(lg > rg || win == 0){
					left.setValue(left.getValue() + 1);
				}else if(rg > lg || win ==1){
					right.setValue(right.getValue() +1);
				}
			}
			ok.setEnabled(false);
			JOptionPane.showMessageDialog(null, "Ergebnis eingetragen");
			dispose();
		}

	}
}
