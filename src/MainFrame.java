import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

//Main screen for the programm

public class MainFrame extends JFrame{
	private JLabel head;
	private JButton insertTeam;
	private JButton setGoal;
	private JButton endGame;
	private JButton showTable;
	private JButton showTeam;
	private JButton showGoals;
	private JLabel referee;
	private JLabel curGame;
	private JLabel nextGame;
	private JLabel groupL;
	private JLabel seatL;
	private JLabel semiL;

	
	private Team[] teams = new Team[8];
	private Team[] groupA = new Team[4];
	private Team[] groupB = new Team[4];

	private String leftName;
	private String rightName;


	private int[][] dv = new int[8][8];

	private LinkedList<Player> sListe = new LinkedList<>();
	private LinkedList<String> ergList = new LinkedList<>();
	//TODO write a log file
	//flags
	private int halb = 0;
	private int counter = 0;//Set counter to 8 for testing if you don't want to simulate Groups xD

	//Init Main menue
	public MainFrame(Team[] teams, int halb){
		this.teams = teams;
		this.halb = halb;

		for(int i = 0; i < 4; i++){
			groupA[i] = teams[i];
			groupB[i] = teams[i +4];
		}

		//create frame
		setSize(1050, 600);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Turnier fuer 8 Mannschaften");
		setResizable(false);
		setLayout(null);

		Font normalFont = new Font("Arial", Font.BOLD, 20);
		Font haedFont = new Font("Arial", Font.BOLD, 50);

		referee = new JLabel("Schiedsrichter: " + groupB[0].getName());
		referee.setBounds(50 , 100, 800, 30);
		referee.setFont(normalFont);
		add(referee);

		curGame = new JLabel("Aktuelle Partie: " + groupA[0].getName() + " : " + groupA[1].getName());
		rightName = groupA[0].getName();
		leftName = groupA[1].getName();
		curGame.setBounds(50, 200, 800, 30);
		curGame.setFont(normalFont);
		add(curGame);

		nextGame = new JLabel("Naechste Partie: " + groupA[2].getName() + " : " + groupA[3].getName());
		nextGame.setBounds(50, 300, 800, 30);
		nextGame.setFont(normalFont);
		add(nextGame);

		head = new JLabel("Wilkommen im TurnierHelper(BETA)");
		head.setBounds(150, 30, 900, 50);
		head.setFont(haedFont);
		add(head);

		insertTeam = new JButton("Spieler einfuegen");
		insertTeam.setBounds(50, 400, 200, 50);
		insertTeam.addActionListener(new ButtonListener());
		add(insertTeam);

		setGoal = new JButton("Tor eintragen");
		setGoal.setBounds(300, 400, 200, 50);
		setGoal.addActionListener(new ButtonListener());
		add(setGoal);

		endGame = new JButton("Endergebnis");
		endGame.setBounds(550, 400, 200, 50);
		endGame.addActionListener(new ButtonListener());
		add(endGame);

		showTable = new JButton("Tabellen anzeigen");
		showTable.setBounds(800, 400, 200, 50);
		showTable.addActionListener(new ButtonListener());
		add(showTable);

		showTeam = new JButton("Mannschaft anzeigen");
		showTeam.setBounds(300, 500, 200, 50);
		showTeam.addActionListener(new ButtonListener());
		add(showTeam);

		showGoals = new JButton("Torschuetzen anzeigen");
		showGoals.setBounds(550, 500, 200, 50);
		showGoals.addActionListener(new ButtonListener());
		add(showGoals);

		groupL = new JLabel("Gruppenspiel 1");
		groupL.setBounds(800, 170, 300, 40);
		groupL.setFont(normalFont);
		add(groupL);

		seatL = new JLabel("Platzierungsspiel");
		seatL.setBounds(800, 220, 300, 40);
		add(seatL);

		if(halb == 1){
			semiL = new JLabel("Halbfinale");
			semiL.setBounds(800, 220, 300, 40);
			add(semiL);
			seatL.setBounds(800,  270,  300,  40);
		}
		setVisible(true);
	}

	private class ButtonListener implements ActionListener{

		//Do actions for different Buttons
		@Override
		public void actionPerformed(ActionEvent arg0) {
			if(counter < 12){
				groupL.setText("Gruppenspiel " + (counter+2));
			}else if(halb==1 && counter < 15){
				groupL.setText("Gruppenphase");
				groupL.setFont(seatL.getFont());
				semiL.setFont(new Font("Arial", Font.BOLD, 20));
				semiL.setText("Halbfinale "+ (counter -11));
			}else{
				groupL.setText("Gruppenphase");
				groupL.setFont(seatL.getFont());
				if(halb ==1){
					semiL.setFont(seatL.getFont());
					semiL.setText("Halbfinale");
				}
				seatL.setFont(new Font("Arial", Font.BOLD, 20));
				seatL.setText("Platzierungsspiel " + (counter -14));
			}
			if(arg0.getActionCommand().equals("Endergebnis") || arg0.getActionCommand().equals("beende hf") || 
					arg0.getActionCommand().equals("beende gf")){
				if(JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(null, "Wirklich eintragen? Es gibt keine Moeglichkeit mehr zurueck!"))
					setEntry();
			}else if(arg0.getActionCommand().equals("Tor eintragen")){
				insertGoal();
			}else if(arg0.getActionCommand().equals("Tabellen anzeigen")){
				showTable();
			}else if(arg0.getActionCommand().equals("Spieler einfuegen")){
				insertPlayer(0);
			}else if(arg0.getActionCommand().equals("Mannschaft anzeigen")){
				insertPlayer(1);
			}else if(arg0.getActionCommand().equals("Torschuetzen anzeigen")){
				showGoalgetters();
			}
		}

	}

//Actions depending on button pressed
	
	
	private void setEntry(){
		counter++;
		endGame.setText("Endergebnis");
		Team[] leftS = {groupA[0], groupA[2], groupB[0], groupB[2], groupA[3], groupA[1], groupB[3],
				groupB[1], groupA[2], groupA[1], groupB[2], groupB[1]};
		Team[] rightS = {groupA[1], groupA[3], groupB[1], groupB[3], groupA[0], groupA[2], groupB[0],
				groupB[2], groupA[0], groupA[3], groupB[0], groupB[3]};
		Team[] referees = {groupB[0], groupB[2], groupA[0], groupA[2], groupB[1], groupB[2], groupA[0],
				groupA[3], groupB[3], groupB[1], groupA[2], groupA[1]};
		if(counter ==12){
			doEntry(leftS[11], rightS[11], counter);
			endGame.setText("beende gf");
			return;
		}

		//Groupphase
		if(counter < 12){
			doEntry(leftS[counter -1], rightS[counter -1], counter);
			curGame.setText("Aktuelle Partie: " + leftS[counter].getName() + " : " + rightS[counter].getName());
			leftName = leftS[counter].getName();
			rightName = rightS[counter].getName();
			if(counter != 11){
				referee.setText("Schiedsrichter: " + referees[counter + 1].getName());
				nextGame.setText("Naechste Partie: " + leftS[counter +1].getName() + " : " + rightS[counter + 1].getName());
			}else{
				referee.setText("Schiedsrichter: Wird berechnet ...");
				nextGame.setText("Naechste Partie: Wird berechnet ...");
			}
		}else if(halb == 1 && counter < 16){
			//Semifinal
			if(counter ==13){
				Arrays.sort(groupA, new TeamComparator(dv));
				Arrays.sort(groupB, new TeamComparator(dv));
				for(int i = 0; i < 4; i++){
					groupA[i].setValue(2 * (3-i));
					groupB[i].setValue(2 * (3-i));
				}
			}

			Team[] left = {groupA[0], groupB[0], groupA[3]};
			Team[] right = {groupB[1], groupA[1], groupB[3]};
			Team[] referee2 = {groupB[0], groupA[1], groupB[2]};
			if(counter < 15){
				referee.setText("Schiedsrichter: " + referee2[counter - 12].getName());
				curGame.setText("Aktuelle Partie: " + left[counter - 13].getName() + " : " + right[counter - 13].getName());
				leftName = left[counter - 13].getName();
				rightName = right[counter -13].getName();
				nextGame.setText("Naechste Partie: " + left[counter - 12].getName() + " : " + right[counter - 12].getName());
			}
			try{
				BufferedWriter bw = new BufferedWriter(new FileWriter("Spielplan.txt"));
				for(int a = 0; a < left.length; a++){
					bw.write(left[a].getName() + " : " + right[a].getName() + "  Schiedsrichter: " + referee2[a].getName());
					bw.newLine();
				}
				bw.close();
			}catch(IOException e){
				System.err.println("Severe error occured during writing the file!! Exit the programm and check your hard disk or RAM!");
			}
			if(counter !=13){
				doEntry(left[counter - 14], right[counter -14], counter);
				if(counter == 15){
					referee.setText("berechne ...");
					nextGame.setText("berechne ...");
					endGame.setText("beende hf");
				}
			}
		}else{
			//KO Phase
			if(counter == 16 || counter ==13){
				Arrays.sort(groupA, new TeamComparator(dv));
				Arrays.sort(groupB, new TeamComparator(dv));
				for(int i = 0; i < 4; i++){
					groupA[i].setValue(2 * (3-i));
					groupB[i].setValue(2 * (3-i));
				}
				Arrays.sort(teams, new TeamComparator(dv));
				counter = 16;
			}

			Team[] left = {groupA[3] , groupB[2], teams[2], teams[1]};
			Team[] right = {groupB[3], groupA[2], teams[3], teams[0]};
			Team[] referee2 = {groupB[1], teams[1], groupA[3], groupA[2]};
			if(counter < 20){
				curGame.setText("Aktuelle Partie: " + left[counter -16].getName() + " : " + right[counter -16].getName());
				leftName = left[counter -16].getName();
				rightName = right[counter - 16].getName();
			}
			if(counter < 19){
				referee.setText("Schiedsrichter: " + referee2[counter - 15].getName());
				nextGame.setText("Naechste Partie: " + left[counter - 15].getName() + " : " + right[counter -15].getName());
			}
			try{
				BufferedWriter b = new BufferedWriter(new FileWriter("Spielplan.txt"));
				for(int k = 0; k < 4; k++){
					b.write(left[k].getName() + " : " + right[k].getName() + "  Schiedsrichter: " + referee2[k].getName());
					b.newLine();
				}
				b.close();
			}catch(IOException e){
				System.err.println("Severe error occured during writing the file!! Exit the programm and check your hard disk or RAM!");
			}
			if(counter > 16){
				doEntry(left[counter -17], right[counter -17], counter);
			}
		}

	}

	private void insertPlayer(int flag){
		String[] names = new String[8];
		for(int i = 0; i < 8; i++){
			names[i] = teams[i].getName();
		}
		new SelectTeamFrame(names, sListe, flag);
	}

	private void showTable(){	
		String[] ops = {"Gruppe A", "Gruppe B", "Alle"};
		int d = JOptionPane.showOptionDialog(null, "Waehle Gruppe:", null, 0, JOptionPane.INFORMATION_MESSAGE, null, ops, null);
		if(d == 0){
			new TableFrame(groupA, dv, "Gruppe A");
		}else if( d== 1){
			new TableFrame(groupB, dv, "Gruppe B");
		}else if(d == 2){
			new TableFrame(teams, dv, "Endstand");
		}
	}

	private void insertGoal(){
		new GoalFrame(sListe, leftName, rightName);
	}

	private void doEntry(Team left, Team right, int counter){
		new EntryFrame(left, right, counter, dv, halb);
	}

	private void showGoalgetters(){
		new GoalgetterFrame(sListe);
	}
}
