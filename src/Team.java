//Team for the programm

public class Team{
	private String name;
	private int points;
	private int goals;
	private int minusgoals;
	private int value;
	private int number;
	
	public Team(String name, int points, int goals, int minusgoals, int value, int number){
			this.name = name;
			this.points = points;
			this.goals = goals;
			this.minusgoals = minusgoals;
			this.value = value;
			this.number = number;
	}
	
	//Getter and Setter
	public String getName(){
		return name;
	}
	public void setName(String name){
		this.name = name;
	}
	
	public int getPoints(){
		return points;
	}
	public void setPoints(int points){
		this.points = points;
	}
	
	public int getGoals(){
		return goals;
	}
	public void setGoals(int goals){
		this.goals = goals;
	}
	
	public int getMinusgoals(){
		return minusgoals;
	}
	public void setMinusgoals(int minusgoals){
		this.minusgoals = minusgoals;
	}
	
	public int getValue(){
		return value;
	}
	public void setValue(int value){
		this.value = value;
	}
	public int getNummer(){
		return number;
	}
	
	//format to print in file with tabstop 8
	public String toString(){
		String ret = name;
		for(int i = 0; i <= (36 - name.length())/8; i++){
			ret += "\t";
		}
		ret += goals + " : " + minusgoals + " " + points;
		return ret;
	}
}